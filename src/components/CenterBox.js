import React from 'react';

class CenterBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            
        }
    }
    

    componentDidMount() {
        
    }

    render() {
        return (
            <div 
                {...this.props} className="centerBox" 
                style={{left:'calc(40% + 1px)',top:'40%', width: 'calc(20% - 1px)', height: 'calc(20% - 1px)' }}
            >
                <div className="red home"></div>
                <div className="green home"></div>
                <div className="yellow home"></div>
                <div className="blue home"></div>
            </div>
        );
    }
}

export default CenterBox;