import React from 'react';
import Dice from './Dice';
import { connect } from 'react-redux'

const mapStateToProps = state => {
    return {
        coinsData: state.coinsData,
        cellArray: state.cellArray,
        canCoinsMove: state.canCoinsMove,
        myColor: state.myColor,
        diceValue: state.diceValue,
        currentColor: state.currentColor
    };
};

class DiceContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            
        }
    }
    

    componentDidMount() {
        
    }

    render() {
        const { color } = this.props;
        const size = Math.round(this.props.boardsize/5);
        return (
            <div className={"diceContainer coloR"+color}>
                <Dice value={this.props.diceValue} size={size}/>
            </div>
        );
    }
}

export default connect(mapStateToProps)(DiceContainer);