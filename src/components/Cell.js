import React from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { updateCellPostion } from '../actions'
import {ArrowForward,StarBorder} from '@material-ui/icons';

const mapDispatchToProps = (dispatch) =>{
    return {
        updatePosition:(position)=>{
            dispatch(updateCellPostion(position));
        }
    }
}

class Cell extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            
        }
    }
    
    updatePosition(boardsize)
    {
        let { status } = this.props;
        if (!status)return;
        var rect = ReactDOM.findDOMNode(this);
        this.props.updatePosition({ i: status.i, j: status.j, positionX: rect.offsetLeft, positionY: rect.offsetTop, boardsize});
    }

    componentDidMount() {
        this.updatePosition(this.props.boardsize);
    }    

    render() {
        const { status } = this.props;
        let className = 'cell',arrow = false,star = false;
        if(status)
        {
            className = 'cell ' + status.cssClass;
            arrow = status.arrow;
            star = status.star;
        }
        return (
            <div className={className} style={{ width: (100 / 15) + '%', height: (100 / 15) + '%' }}>
                {arrow ? <ArrowForward className="icon"/>:null}
                {star ? <StarBorder className="icon"/>:null}
            </div>
        );
    }
}

export default connect(null,mapDispatchToProps)(Cell);