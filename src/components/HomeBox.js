import React from 'react';
import PlayerStartCircle from './PlayerStartCircle';

class HomeBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            
        }
    }
    

    componentDidMount() {
        
    }

    render() {
        const {color} = this.props;
        return (
            <div {...this.props} style={{ width: '40%', height: '40%' }}>
                <div className="homeInner">
                    <PlayerStartCircle color={color} index={0} />
                    <PlayerStartCircle color={color} index={1} />
                    <PlayerStartCircle color={color} index={2} />
                    <PlayerStartCircle color={color} index={3} />
                </div>
            </div>
        );
    }
}

export default HomeBox;