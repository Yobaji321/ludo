import React from 'react';
import { connect } from 'react-redux';
import { localData, movableCoins, checkIfStriked, isWon, isAllWon } from '../actions/helpers'
import { moveCoinNextCell, checkForStrike } from '../actions'
import DiceSprite from '../assets/images/dice_sprite.png';

const mapStateToProps = (state) => {
    return {
        socket: state.socket,
        cellArray: state.cellArray,
        isCoinMoving: state.isCoinMoving,
        coinsData: state.coinsData,
        roomId: state.roomId,
        myColor: state.myColor,
        diceValue: state.diceValue,
        canCoinsMove: state.canCoinsMove,
        usersInRoom: state.usersInRoom,
        currentColor: state.currentColor
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        startMoveCoins: (color, index) => {
            return dispatch({ type: 'SEND_MOVE_DATA_SOCKET', payload: { color, index } });
        },
        changeDiceValue: (diceValue) => {
            return dispatch({ type: 'CHANGE_PROPERTY', payload: { propertyName: 'diceValue', value: diceValue } });
        },
        allowMove: () => {
            return dispatch({ type: 'CHANGE_PROPERTY', payload: { propertyName: 'canCoinsMove', value: true } });
        },
        disableMove: () => {
            return dispatch({ type: 'CHANGE_PROPERTY', payload: { propertyName: 'canCoinsMove', value: false } });
        },
        savePreviousDiceValues: (previousDiceValues) => {
            return dispatch({ type: 'CHANGE_PROPERTY', payload: { propertyName: 'previousDiceValues', value: previousDiceValues } });
        },
        nextPlayersTurn: () => {
            return dispatch({ type: 'NEXT_PLAYER_TURN', payload: {} });
        },
        moveCoinToNextCell: (coinColor, coinIndex) => {
            return dispatch(moveCoinNextCell(coinColor, coinIndex));
        },
        changeIsCoinMoving: (moving) => {
            return dispatch({ type: 'CHANGE_PROPERTY', payload: { propertyName: 'isCoinMoving', value: moving } });
        },
        checkForStrike: (color, index) => {
            dispatch(checkForStrike(color, index));
        },
    }
}

class Dice extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: props.value ?props.value:1,
            isRolling: props.isRolling ? props.isRolling:false,
            currentFrame:1,
            animatingFrames:24,
            size: props.size ? props.size : 40,
        }
    }

    rollingAnimation = null;
    previousDiceValues = [];
    diceTracker = [];

    startMove = (color, index) => {
        this.props.disableMove();
        let isAtHome = (this.props.coinsData[color][index].i === -2);
        this.props.moveCoinToNextCell(color, index);
        let counter = 1;
        let diceValue = this.props.diceValue;
        if (isAtHome) diceValue = 1;
        let previousDiceValues = this.previousDiceValues;
        let lastValue = previousDiceValues[previousDiceValues.length - 1];
        let secondLastValue = previousDiceValues[previousDiceValues.length - 2];
        let thirdLastValue = previousDiceValues[previousDiceValues.length - 3];
        this.props.changeIsCoinMoving(true);
        let intervalId = setInterval(() => {
            counter++;
            if (counter > diceValue) {
                var striked = checkIfStriked(this.props.cellArray, this.props.coinsData, color, index);
                var woN = isWon(this.props.coinsData, color, index);
                this.props.checkForStrike(color, index);
                clearInterval(intervalId);
                this.props.changeIsCoinMoving(false);
                if (!woN && !striked && (this.props.diceValue !== 6 || (secondLastValue === 6 && lastValue === 6 && thirdLastValue === 6)))
                {
                    previousDiceValues[previousDiceValues.length - 1] = 0;
                    this.props.savePreviousDiceValues(previousDiceValues);
                    this.nextPlayersTurn();
                }
                else {
                    this.props.changeDiceValue(0);
                }
            }
            else {
                this.props.moveCoinToNextCell(color, index);
            }
        }, 300);
    }


    nextPlayersTurn = () => {
        var currentColor = this.props.currentColor;
        var COLORS = this.props.usersInRoom.map((user) => { return (user.color) });
        COLORS = COLORS.filter((clr) => {
            return (!isAllWon(this.props.coinsData, clr))
        });
        let nextColorIndex = COLORS.indexOf(currentColor) + 1;
        if (nextColorIndex > (COLORS.length - 1)) nextColorIndex = 0;
        let nextColor = COLORS[nextColorIndex];
        this.props.nextPlayersTurn();
        this.props.socket.emit(this.props.roomId,
            { coinsData: this.props.coinsData, currentColor: nextColor });
        const user = localData.getData('user');
        this.props.socket.emit(this.props.roomId,{ user,currentColor: nextColor });
    }

    playMyTurn = () => {
        var { coinsData } = this.props;
        var { currentColor } = this.props;
        if (this.props.canCoinsMove || this.state.isRolling || this.props.isCoinMoving || currentColor !== this.props.myColor)return;
        this.setState({ isRolling: true });
        var myTurnValue = parseInt(Math.random() * 6 + 1);        
        let previousDiceValues = this.previousDiceValues;
        if (previousDiceValues.indexOf(6) < 0 && previousDiceValues.length >= 10 && previousDiceValues.length >= 10)
        {
            myTurnValue = 6;
        }
        if (previousDiceValues.indexOf(1) < 0 && (Math.random() * 10) < 3 && previousDiceValues.length >= 10) 
        {
            myTurnValue = 1;
        }
        if(window.turn)myTurnValue=window.turn;

        let trackerLastValue = this.diceTracker[this.diceTracker.length - 1];
        let trackerSecondLastValue = this.diceTracker[this.diceTracker.length - 2];

        if(trackerLastValue<=3 && trackerSecondLastValue<=3)
        {
            myTurnValue = parseInt((Math.random() * 3) + 4);
            console.log("no small more");
        }

        this.previousDiceValues.push(myTurnValue);
        if (this.previousDiceValues.length > 10) 
        {
            this.previousDiceValues.splice(0, 1);
        }

        this.diceTracker.push(myTurnValue);
        if (this.diceTracker.length > 10) 
        {
            this.diceTracker.splice(0, 1);
        } 
        this.props.savePreviousDiceValues(this.previousDiceValues);
        var movableCOINS = movableCoins(coinsData[currentColor], currentColor, myTurnValue);
        var ISstuck = (movableCOINS.length === 0);
        let lastValue = previousDiceValues[previousDiceValues.length - 1];
        let secondLastValue = previousDiceValues[previousDiceValues.length - 2];
        let thirdLastValue = previousDiceValues[previousDiceValues.length - 3];
        setTimeout(() => {
            this.setState({ isRolling: false });
            this.props.changeDiceValue(myTurnValue);
            if (!ISstuck && !(secondLastValue === 6 && lastValue === 6 && thirdLastValue === 6))
            {
                this.props.allowMove();
                if (movableCOINS.length === 1)
                {
                    setTimeout(() => {
                        this.startMove(currentColor, movableCOINS[0].index);
                        this.props.startMoveCoins(currentColor, movableCOINS[0].index);
                    }, 400);
                }
            }
            else
            {
                if (secondLastValue === 6 && lastValue === 6 && thirdLastValue === 6)
                {
                    previousDiceValues[previousDiceValues.length - 1] = 0;
                    this.props.savePreviousDiceValues(previousDiceValues);
                }
                this.props.changeIsCoinMoving(true);
                setTimeout(() => {
                    this.props.changeIsCoinMoving(false);
                    this.nextPlayersTurn();
                }, 800);
            }
        }, 500);
        const user = localData.getData('user');
        this.props.socket.emit(this.props.roomId, { user,diceValue: myTurnValue});
    }
    
    nextFrame = ()=>{
        let nextFrame = this.state.currentFrame + 1;
        if (nextFrame>this.state.animatingFrames)
        {
            nextFrame = 1;
        }
        this.setState({currentFrame:nextFrame});
    }

    startRolling = () => {
        clearInterval(this.rollingAnimation);
        this.rollingAnimation = setInterval(() => {
            this.nextFrame();
        }, 80);
    }

    stopRolling = ()=>{
        clearInterval(this.rollingAnimation);
    }

    componentDidUpdate(props,state) {
        if(state.isRolling !== this.state.isRolling)
        {
            if(!this.state.isRolling)
            {
                this.stopRolling();
            }
            else
            {
                this.startRolling();
            }
        }
    }

    componentDidMount() {
        this.startRolling();
        var props = this.props;
        const user = localData.getData('user');
        props.socket.on(props.roomId, (data) => {
            if (user.id !== data.user.id && data.diceValue) {
                    this.setState({ isRolling: true});
                    setTimeout(() => {
                        this.setState({ isRolling: false });
                        this.props.changeDiceValue(data.diceValue);
                    }, 500);
                }
        });
    }

    componentWillReceiveProps(newProps)
    {
        const oldProps = this.props;
        this.setState({
            value: (newProps.value !== oldProps.value) ? newProps.value : oldProps.value,
            isRolling: (newProps.isRolling !== oldProps.isRolling) ? newProps.isRolling : oldProps.isRolling,
            size: (newProps.size !== oldProps.size) ? newProps.size : oldProps.size,
        });
        // if(newProps.value === 0 && oldProps.value !== 0)
        // {
        //     this.setState({ isRolling: true });
        //     setTimeout(() => {
        //         this.setState({ isRolling: false });
        //     }, 500);
        // }
    }

    render() {
        const {size} = this.state;
        const {value} = this.state;
        let offset = -((this.state.currentFrame-1)*size);
        let classValue = ' value'+value;
        let spinClass = ' spin';
        if (this.props.currentColor === this.props.myColor)
        {
            classValue += ' myTurn';
        }
        if(!this.state.isRolling)
        {
            spinClass = '';
            offset = -((this.state.animatingFrames+value-1)*size);
        }
        if (value === 0 && !this.state.isRolling)
        {
            offset = -(20 * size);
        }
        return (
            <div onClick={this.playMyTurn} className={"dice" + spinClass + classValue} style={
                    { 
                        backgroundPosition:offset+'px 0',
                        width:size,height:size,
                        backgroundImage: 'url(' + DiceSprite+')'
                    }
                 }>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dice);