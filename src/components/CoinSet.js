import React from 'react';
import { connect } from 'react-redux'
import { localData, checkIfStriked, isCoinMovable,isWon,isAllWon } from '../actions/helpers'
import { moveCoinNextCell, checkForStrike} from '../actions'

const mapStateToProps = state => {
    return { 
        coinsData: state.coinsData,
        usersInRoom: state.usersInRoom,
        socket: state.socket,
        roomId: state.roomId,
        cellArray: state.cellArray,
        canCoinsMove: state.canCoinsMove,
        myColor: state.myColor,
        diceValue: state.diceValue,
        currentColor: state.currentColor,
        previousDiceValues: state.previousDiceValues
    };
};

const mapStateToPropsForSet = state => {
    return { 
        usersInRoom: state.usersInRoom
    };
};

const mapDispatchToProps = dispatch => {
    return {
        savePreviousDiceValues:(previousDiceValues) => {
            return dispatch({ type: 'CHANGE_PROPERTY', payload: { propertyName: 'previousDiceValues', value: previousDiceValues } });
        },
        changeDiceValue:(diceValue) => {
            return dispatch({ type: 'CHANGE_PROPERTY', payload: { propertyName: 'diceValue', value: diceValue } });
        },
        changeIsCoinMoving:(moving) => {
            return dispatch({ type: 'CHANGE_PROPERTY', payload: { propertyName: 'isCoinMoving', value: moving } });
        },
        moveCoinToNextCell: ( coinColor, coinIndex)=>{
            dispatch(moveCoinNextCell(coinColor,coinIndex));
        },
        checkForStrike: (color,index)=>{
            dispatch(checkForStrike(color, index));
        }, 
        startMoveCoins:(color, index) => {
            return dispatch({ type: 'SEND_MOVE_DATA_SOCKET', payload: { color, index} });
        },
        disableMove:() => {
            return dispatch({ type: 'CHANGE_PROPERTY', payload: { propertyName: 'canCoinsMove', value: false } });
        },
        nextPlayersTurn:() => {
            return dispatch({ type: 'NEXT_PLAYER_TURN', payload: {} });
        }
    }
}

class Coin extends React.Component {

    nextPlayersTurn = () => {
        var currentColor = this.props.currentColor;
        var COLORS = this.props.usersInRoom.map((user) => { return (user.color) });
        COLORS = COLORS.filter((clr) => {
            return (!isAllWon(this.props.coinsData, clr))
        });
        let nextColorIndex = COLORS.indexOf(currentColor) + 1;
        if (nextColorIndex > (COLORS.length - 1)) nextColorIndex = 0;
        let nextColor = COLORS[nextColorIndex];
        this.props.nextPlayersTurn();
        this.props.socket.emit(this.props.roomId,
            { coinsData: this.props.coinsData, currentColor: nextColor });
        const user = localData.getData('user');
        this.props.socket.emit(this.props.roomId,{ user,currentColor: nextColor });
    }

    startMove = (color, index)=>{
        this.props.disableMove();
        let isAtHome = (this.props.coinsData[color][index].i === -2);
        this.props.moveCoinToNextCell(color, index);
        let counter = 1;
        let diceValue = this.props.diceValue;
        if(isAtHome)diceValue = 1;
        let previousDiceValues = this.props.previousDiceValues;
        let lastValue = previousDiceValues[previousDiceValues.length-1];
        let secondLastValue = previousDiceValues[previousDiceValues.length - 2];
        let thirdLastValue = previousDiceValues[previousDiceValues.length - 3];
        this.props.changeIsCoinMoving(true);
        let intervalId = setInterval(() => {
            counter++;
            if (counter > diceValue)
            {
                var striked = checkIfStriked(this.props.cellArray, this.props.coinsData, color, index);
                var woN = isWon(this.props.coinsData, color, index);
                this.props.checkForStrike(color, index);
                clearInterval(intervalId);
                this.props.changeIsCoinMoving(false);
                if (!woN && !striked && (this.props.diceValue !== 6 || (secondLastValue === 6 && lastValue === 6 && thirdLastValue === 6)))
                {
                    previousDiceValues[previousDiceValues.length - 1] = -1;
                    this.props.savePreviousDiceValues(previousDiceValues);
                    this.nextPlayersTurn();
                }
                else
                {
                    this.props.changeDiceValue(0);
                }
            }
            else
            {
                this.props.moveCoinToNextCell(color, index);
            }
        }, 300);
    }

    findCoinPlace()
    {
        const { color } = this.props;
        const { index } = this.props, colors = ['red', 'green', 'yellow','blue'];
        const coinData = this.props.coinsData[color];
        const i = coinData[index].i, j = coinData[index].j;
        let coinsInPlace = 0,coinsRank = 1,eachCoinsPlace={};
        colors.map((COLOR)=>{
            eachCoinsPlace[COLOR] = 0;
            this.props.coinsData[COLOR].map((position,coinIndex)=>{
                if(position.i === i && position.j === j && i !== -2)
                {
                    coinsInPlace++;
                    eachCoinsPlace[COLOR]++;
                    if(color === COLOR && index === coinIndex)
                        {
                            coinsRank=coinsInPlace;
                        }
                }
                return true;
            });
            return true;
        });
        return { coinsInPlace, coinsRank, eachCoinsPlace};
    }

    render() {
        const {color} = this.props;
        const {index} = this.props;
        const { diceValue } = this.props;
        const coinData = this.props.coinsData[color];
        const i = coinData[index].i, j = coinData[index].j;
        const coinsInThisCell = this.findCoinPlace();
        let left = 0, top = 0, startClass='', scale = 0.8;
        if (i !== -1 && i !== -2 && this.props.cellArray[i] && this.props.cellArray[i][j])
        {
            left = this.props.cellArray[i][j].positionX;
            top = this.props.cellArray[i][j].positionY;
            if (this.props.cellArray[i][j].startCell)
            {
                startClass = this.props.cellArray[i][j].startCell;
            }
        }
        if(coinsInThisCell.coinsInPlace>1)
        scale = 0.9/(coinsInThisCell.coinsInPlace);
        let offset = 0;
        let middleCoin = Math.round(coinsInThisCell.coinsInPlace / 2);
        if (coinsInThisCell.coinsInPlace%2 === 0)middleCoin+=0.5;

        offset = (middleCoin - coinsInThisCell.coinsRank)*(scale*7);
        let style = { 
            left: left + 0.08, 
            top: top - 0.08, 
            transform:'scale('+scale+')' 
        };
        if (!((j > 5 && j < 9) && (i > 5 && i < 9)))
        {
            if (i < 9 && i > 5) style.top += offset;
            else style.left += offset;
        }
        else
        {
            offset = (middleCoin - coinsInThisCell.coinsRank) * (scale * 11);
            if (i === 7) style.top += offset;
            else style.left += offset;
            if (coinsInThisCell.coinsInPlace>1)scale+=0.1;
        }
        style.top += '%';
        style.left += '%';
        style.transform = 'scale(' + scale + ')' ;
        if (i === -2) style = {};
        let activeClass = '';
        let wonClass = '';

        if (this.props.currentColor === color && 
            color === this.props.myColor && 
            this.props.canCoinsMove && !((j > 5 && j < 9) && (i > 5 && i < 9)))
        {
            activeClass = 'ready ';
        }
        if(((j > 5 && j < 9) && (i > 5 && i < 9)))
        {
            wonClass = 'won ';
        }
        if ((diceValue !== 6 && i === -2) || !isCoinMovable(color,i,j,diceValue))
        {
            activeClass = '';
        } 
        return (
            <div 
                onClick={()=>{
                    if (!(this.props.currentColor === color &&
                        color === this.props.myColor &&
                        this.props.canCoinsMove && isCoinMovable(color, i, j, diceValue) && !((j > 5 && j < 9) && (i > 5 && i < 9))) || (diceValue !== 6 && i === -2))return;
                    this.startMove(color, index);
                    this.props.startMoveCoins(color, index);
                }}
                className={"coin " + wonClass + activeClass + startClass + color + ((i===-2)?' home':'')+' circle'+index} 
                style={style} >
            </div>
        );
    }
}

function CoinSet(props)
{
    const COIN = connect(mapStateToProps, mapDispatchToProps)(Coin);
    var COLORS = props.usersInRoom.map((user) => { return (user.color) }); 
    if(COLORS.indexOf(props.color)<0)
    {
        return null;
    }
    return(
        <div>
            {[0,1,2,3].map((item,i)=>{
                return (<COIN coinSize={props.coinSize} key={i} color={props.color} index={i}/>);
            })}
        </div>
    );
}

export default connect(mapStateToPropsForSet,mapDispatchToProps)(CoinSet);