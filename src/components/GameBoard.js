import React from 'react';
import {connect} from 'react-redux';
import Cell from './Cell'
import HomeBox from './HomeBox'
import CenterBox from './CenterBox'
import { initCells } from '../actions'
import CoinSet from './CoinSet'
import DiceContainer from './DiceContainer'

const mapDispatchToProps = (dispatch)=>{
    return { initCells: ()=>{
        return dispatch(initCells())
    }
}
}
const mapStateToProps = (state)=>{
    return { cellArray: state.cellArray, currentColor: state.currentColor}
}
class GameBoard extends React.Component{
    constructor(props)
    {
        super(props);
        this.state = {
            size:0,
            offset:5
        }
    }

    calculateSize = () =>
    {
        let heightToMap = window.innerHeight;
        if(window.innerHeight > window.innerWidth)
        {
            heightToMap = window.innerWidth;
        }
        this.setState({ size: heightToMap - (this.state.offset*2)});
    }

    componentDidMount()
    {
        this.calculateSize();
        this.props.initCells();
        window.addEventListener('resize', this.calculateSize);
    }

    render()
    {
        const {cellArray} = this.props;
        const color = this.props.color;
        const rotateAngle = ['blue','red','green','yellow'].indexOf(color)*-90;
        const boardStyle = { transform:'rotate('+rotateAngle+'deg)',width: this.state.size, height: this.state.size, marginTop:this.state.offset };
        if (!color)return null;
        return(
            <div className={"gameBoard coloR"+color} style={boardStyle}>
                <DiceContainer boardsize={this.state.size} color={this.props.currentColor}/>
                <CoinSet color="red" />
                <CoinSet color="green" />
                <CoinSet color="blue" />
                <CoinSet color="yellow" />
                <HomeBox className="homeBox red" color="red"/>
                <HomeBox className="homeBox green" color="green"/>
                <HomeBox className="homeBox blue" color="blue"/>
                <HomeBox className="homeBox yellow" color="yellow"/>
                <CenterBox/>
                {
                    cellArray.map((cellRow)=>{
                        
                            return (cellRow.map((status,j)=>{
                                return (<Cell boardsize={this.state.size} status={status} key={j} />);
                            }));
                        
                    })
                }
            </div>
        );
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(GameBoard);