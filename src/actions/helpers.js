export function getNextCellIndex(i, j, color) {
    let iNew = i, jNew = j;
    const startPositions = {
        red:{i:6,j:1},
        green:{i:1,j:8},
        yellow:{i:8,j:13},
        blue:{i:13,j:6}
    };
    if(i===-2)
    {
        iNew = startPositions[color].i;
        jNew = startPositions[color].j;
    }
    else if ((j > 5 && j < 9) && (i > 5 && i < 9))
    {
        return { i, j };
    }
    else if (color === 'red' && i === 7 && j < 6) {
        if (j !== 6) {
            jNew++;
        }
    }
    else if (color === 'yellow' && i === 7 && j > 8) {
        if (j !== 8) {
            jNew--;
        }
    }
    else if (color === 'green' && j === 7 && i < 6) {
        if (i !== 6) {
            iNew++;
        }
    }
    else if (color === 'blue' && j === 7 && i > 8) {
        if (i !== 8) {
            iNew--;
        }
    }
    else if (i === 8 && j === 9) {
        iNew = 9;
        jNew = 8;
    }
    else if (i === 9 && j === 6) {
        iNew = 8;
        jNew = 5;
    }
    else if (i === 6 && j === 5) {
        iNew = 5;
        jNew = 6;
    }
    else if (i === 5 && j === 8) {
        iNew = 6;
        jNew = 9;
    }
    else if (j === 14 && i !== 8) {
        iNew++;
    }
    else if (i === 6) {
        jNew++;
    }
    else if (j === 0) {
        iNew--;
    }
    else if (i === 8) {
        jNew--;
    }
    else if (j === 14) {
        jNew--;
    }
    else if (i === 14 && j !== 6) {
        jNew--;
    }
    else if (j === 8) {
        iNew++;
    }
    else if (i === 0) {
        jNew++;
    }
    else if (j === 6) {
        iNew--;
    }
    return { i:iNew, j:jNew }
}

export function getPrevCellIndex(i, j, color) {
    let iNew = i, jNew = j;
    const startPositions = {
        red: { i: 6, j: 1 },
        green: { i: 1, j: 8 },
        yellow: { i: 8, j: 13 },
        blue: { i: 13, j: 6 }
    };
    if (i === startPositions[color].i && j === startPositions[color].j) {
        iNew = -2;
        jNew = -2;
    }
    else if (i === 9 && j === 8) {
        iNew = 8;
        jNew = 9;
    }
    else if (i === 8 && j === 5) {
        iNew = 9;
        jNew = 6;
    }
    else if (i === 5 && j === 6) {
        iNew = 6;
        jNew = 5;
    }
    else if (i === 6 && j === 9) {
        iNew = 5;
        jNew = 8;
    }
    else if (j === 14 && i !== 6) {
        iNew--;
    }
    else if (i === 6 && j !== 0) {
        jNew--;
    }
    else if (i === 8) {
        jNew++;
    }
    else if (j === 0) {
        iNew++;
    }
    else if (j === 14) {
        jNew++;
    }
    else if (i === 14 && j !== 8) {
        jNew++;
    }
    else if (i === 0 && j!==6) {
        jNew--;
    }
    else if (j === 8) {
        iNew--;
    }
    else if (j === 6) {
        iNew++;
    }
    return { i: iNew, j: jNew }
}

export function homePathCells(i, j, color)
{
    let pathEndPoints = ['146','148','98','89','814','614','69','58','08','06','56','65','60','80','85','96'];

    const startPositions = {
        red: { i: 6, j: 1 },
        green: { i: 1, j: 8 },
        yellow: { i: 8, j: 13 },
        blue: { i: 13, j: 6 }
    };

    const startPos = startPositions[color];

    let homePathPoints = [];

    let prevPosition = getPrevCellIndex(i,j,color);

    while(prevPosition.i !== startPos.i || prevPosition.j !== startPos.j)
    {
        if(pathEndPoints.indexOf(prevPosition.i+''+prevPosition.j)>=0)
        {
            homePathPoints.push({i:prevPosition.i,j:prevPosition.j});
        }
        prevPosition = getPrevCellIndex(prevPosition.i, prevPosition.j, color);
    }
    return homePathPoints;
    
}

export function isWon(coinsData, color, index)
{
    let i = coinsData[color][index].i;
    let j = coinsData[color][index].j;
    if (((j > 5 && j < 9) && (i > 5 && i < 9))) {
        return(true);
    }
    return false;
}

export function isAllWon(coinsData,color)
{
    return([0,1,2,3].filter((index)=>{return(isWon(coinsData,color,index))}).length===4)
}

export function checkIfStriked(cellArray,coinsData,color,index)
{
    let i = coinsData[color][index].i;
    let j = coinsData[color][index].j;    
    if(i===-2)return;
    const positionToCheck = { i, j };
    let colorToStrike = false, strikeColorCount = 0;
    ['red', 'green', 'yellow', 'blue'].map((COLOR) => {
        if (COLOR === color) return true;
        coinsData[COLOR].map((position, index) => {
            if (position.i === positionToCheck.i && position.j === positionToCheck.j) {
                colorToStrike = COLOR;
                strikeColorCount++;
            }
            return true;
        });
        return true;
    });
    if (colorToStrike && strikeColorCount % 2 !== 0) {
        const status = cellArray[positionToCheck.i][positionToCheck.j];
        if (!status.star && !status.startCell) {
            return true;
        }
    }
    return false;
}

export function movableCoins(coinData, color, diceValue) 
{
    return coinData.map((position, i) => {
        if (isCoinMovable(color, position.i, position.j, diceValue))
            return { index: i, position };
        else
            return null;
    }).filter((position) => { return (position !== null) });
}

export function isCoinMovable(color,i,j,diceValue)
{
    if(diceValue!==6&&i===-2)return false;
    const finalPaths = {
        red:    ['70','71','72','73','74','75','76'],
        green:  ['07','17','27','37','47','57','67'],
        yellow: ['714','713','712','711','710','79','78'],
        blue:   ['147','137','127','117','107','97','87']
    };

    var currentPos = i+''+j;
    var inFinalPath = finalPaths[color].indexOf(currentPos);
    
    if(inFinalPath<0)return true;

    if ((6 - inFinalPath) >= diceValue) return true;

    return false;
}

const localData = {
    getData:(key)=>{
        return(JSON.parse(localStorage.getItem(key)));
    },
    setData:(key,data)=>{
        return(localStorage.setItem(key,JSON.stringify(data)));
    }
}

export { localData }