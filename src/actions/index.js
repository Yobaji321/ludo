export function initCells() {
    let cellArray = [];

    for (let i = 0; i < 15; i++) {
        cellArray.push([]);
        for (let j = 0; j < 15; j++) {
            let status = false;
            
            if (((j > 5 && j < 9) || (i > 5 && i < 9)) && !((j > 6 && j < 8) && (i > 6 && i < 8)))
            {
                let cssClass = 'path';
                if (i === 7 && j < 6) cssClass += ' red';
                if (i === 7 && j > 8) cssClass += ' yellow';
                if (j === 7 && i < 6) cssClass += ' green';
                if (j === 7 && i > 8) cssClass += ' blue';

                if (i === 6 && j === 1) cssClass += ' red';
                if (i === 8 && j === 13) cssClass += ' yellow';
                if (j === 8 && i === 1) cssClass += ' green';
                if (j === 6 && i === 13) cssClass += ' blue';
                status = 
                { 
                    players: [], 
                    i,
                    j, 
                    cssClass, 
                    arrow: false, 
                    star: false,
                    startCell: false,
                    positionX:null,
                    positionY:null 
                    };
                if (['136', '61', '18', '813'].indexOf(i + '' + j) >= 0) {
                    status.cssClass += ' startCell';
                    status.startCell = cssClass+'startCell ';
                }
                if (['70', '07', '714', '147'].indexOf(i + '' + j) >= 0)
                {
                    status.cssClass += ' arrow arrow'+i+j;
                    status.arrow = true;
                }
                if (['82', '26', '612', '128'].indexOf(i + '' + j) >= 0) {
                    status.cssClass += ' star';
                    status.star = true;
                }
            }
            cellArray[i].push(status);
        }
    }
    return { type: 'INIT_CELLS', payload:cellArray };
}

export function moveCoinNextCell(coinColor,coinIndex)
{
    return { type: 'NEXT_COIN_POSITION', payload: { color: coinColor, index: coinIndex}}
}

export function updateCellPostion(position)
{
    return { type: 'SAVE_CELL_POSITION', payload: position };
}

export function checkForStrike(color, index)
{
    return { type: 'CHECK_FOR_STRIKE', payload: { color, index} };
}