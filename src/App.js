import React from 'react';
import GameBoard from './components/GameBoard';
import { connect } from 'react-redux';
import './assets/css/ludo.css';
import { localData, checkIfStriked } from './actions/helpers'
import { moveCoinNextCell, checkForStrike } from './actions'
import uuidv4 from 'uuid/v4'

const mapStateToProps = (state) => {
	return { previousDiceValues: state.previousDiceValues,cellArray: state.cellArray,socket: state.socket, diceValue: state.diceValue, coinsData: state.coinsData, roomId: state.roomId, myColor: state.myColor }
}

const mapDispatchToProps = (dispatch) => {
	return {
		savePreviousDiceValues: (previousDiceValues) => {
			return dispatch({ type: 'CHANGE_PROPERTY', payload: { propertyName: 'previousDiceValues', value: previousDiceValues } });
		},
		changeIsCoinMoving: (moving) => {
			return dispatch({ type: 'CHANGE_PROPERTY', payload: { propertyName: 'isCoinMoving', value: moving } });
		},
		changeDiceValue:(diceValue) => {
			return dispatch({ type: 'CHANGE_PROPERTY', payload: { propertyName: 'diceValue', value: diceValue } });
		},
		updateCoin:(coinsData) => {
			return dispatch({ type: 'CHANGE_PROPERTY', payload: { propertyName: 'coinsData', value: coinsData } });
		},
		saveRoomId: (roomId) => {
			return dispatch({ type: 'CHANGE_PROPERTY', payload: { propertyName: 'roomId', value: roomId } });
		},
		saveMyColor: (color) => {
			return dispatch({ type: 'CHANGE_PROPERTY', payload: { propertyName: 'myColor', value: color } });
		},
		saveCurrentColor: (color) => {
			return dispatch({ type: 'CHANGE_PROPERTY', payload: { propertyName: 'currentColor', value: color } });
		},
		saveUsersInRoom: (users) => {
			return dispatch({ type: 'CHANGE_PROPERTY', payload: { propertyName: 'usersInRoom', value: users } });
		},
		nextPlayersTurn: () => {
			return dispatch({ type: 'NEXT_PLAYER_TURN', payload: {} });
		},
		moveCoinToNextCell: (coinColor, coinIndex) => {
			dispatch(moveCoinNextCell(coinColor, coinIndex));
		},
		checkForStrike: (color, index) => {
			dispatch(checkForStrike(color, index));
		}
	}
}

class App extends React.Component {

	constructor(props) {
		super(props);
		var user = localData.getData('user');
		if (!user) {
			user = { name: '', id: uuidv4() };
			localData.setData('user', user);
		}

		props.socket.on(user.id, (data) => {
			this.props.updateCoin(data.roomData.coinsData);
			this.props.saveRoomId(data.roomId);
			var usersInRoom = data.roomData.users;
			this.props.saveUsersInRoom(usersInRoom);
			var myUserIndex = usersInRoom.findIndex((USER) => { return (USER.id === user.id) });
			this.props.saveMyColor(usersInRoom[myUserIndex].color);
			this.props.saveCurrentColor(data.roomData.currentColor);
		});

		props.socket.emit('newUser', { user, coinsData: props.coinsData });

		props.socket.on('usersList', (data) => {
			//console.log(data);
		});

	}

	startMove = (color, index) => {
		let isAtHome = (this.props.coinsData[color][index].i === -2);
		this.props.moveCoinToNextCell(color, index);
		var counter = 1;
		let diceValue = this.props.diceValue;
		let previousDiceValues = this.props.previousDiceValues;
		if (isAtHome) diceValue = 1;
		this.props.changeIsCoinMoving(true);
		var intervalId = setInterval(() => {
			counter++;
			if (counter > diceValue) {
				var gotStriked = checkIfStriked(this.props.cellArray, this.props.coinsData, this.props.myColor, index);
				if (gotStriked)
				{
					previousDiceValues.push(6);
					if (previousDiceValues.length > 10) 
					{
						previousDiceValues.splice(0, 1);
					}
					this.props.savePreviousDiceValues(previousDiceValues);
				}
				this.props.checkForStrike(color, index);
				clearInterval(intervalId);
				this.props.changeIsCoinMoving(false);
			}
			else {
				this.props.moveCoinToNextCell(color, index);
			}
		}, 300);
	}

	componentWillReceiveProps(props) {
		if (props.roomId !== this.props.roomId && props.roomId !== '') {
			const user = localData.getData('user');
			props.socket.on(props.roomId, (data) => {
				//console.log(data)
				if (user.id !== data.user.id) {
					if (data.coinsData) {
						this.props.updateCoin(data.coinsData);
						this.props.saveCurrentColor(data.roomData.currentColor);
					}
					else if (data.startMove) {
						this.props.changeDiceValue(data.startMove.diceValue);
						this.startMove(data.startMove.color, data.startMove.index);
					}
					else if (data.usersInRoom) {
						this.props.saveUsersInRoom(data.usersInRoom);
					}
					else if (data.currentColor) {
						console.log(data);
						this.props.saveCurrentColor(data.currentColor);
						this.props.changeDiceValue(0);
					}
				}
			});
		}
	}

	render() {
		return (
			<div className="boardContainer">
				<GameBoard color={this.props.myColor} />
			</div>
		);
	}
}
export default connect(mapStateToProps, mapDispatchToProps)(App);
