import { getNextCellIndex } from '../actions/helpers'
import { localData, isAllWon } from '../actions/helpers'
import openSocket from 'socket.io-client'
import config from '../config'

const socket = openSocket(config.socketUrl);

const initialState = {
    socket,
    usersInRoom:[],
    myColor:null,
    canCoinsMove:false,
    isCoinMoving:false,
    diceValue:0,
    currentColor: null,
    cellArray: [[]],
    roomId:'',
    previousDiceValues:[],
    coinsData:{
        red:[
            {i:-2,j:-2,positionX:0,positionY:0},
            {i:-2,j:-2,positionX:0,positionY:0},
            {i:-2,j:-2,positionX:0,positionY:0},
            {i:-2,j:-2,positionX:0,positionY:0}
        ],
        green:[
            {i:-2,j:-2,positionX:0,positionY:0},
            {i:-2,j:-2,positionX:0,positionY:0},
            {i:-2,j:-2,positionX:0,positionY:0},
            {i:-2,j:-2,positionX:0,positionY:0}
        ],
        yellow:[
            {i:-2,j:-2,positionX:0,positionY:0},
            {i:-2,j:-2,positionX:0,positionY:0},
            {i:-2,j:-2,positionX:0,positionY:0},
            {i:-2,j:-2,positionX:0,positionY:0}
        ],
        blue:[
            {i:-2,j:-2,positionX:0,positionY:0},
            {i:-2,j:-2,positionX:0,positionY:0},
            {i:-2,j:-2,positionX:0,positionY:0},
            {i:-2,j:-2,positionX:0,positionY:0}
        ]
    }
};

function rootReducer(state = initialState, action) {
    if (action.type === 'INIT_CELLS') {
        return {...state, cellArray: action.payload};
    }
    if (action.type === 'SAVE_CELL_POSITION') {
        let newCellArray = state.cellArray.slice();
        const newPosition = action.payload;
        let newXperc = (newPosition.positionX / newPosition.boardsize)*100;
        let newYperc = (newPosition.positionY / newPosition.boardsize)*100;
        newCellArray[newPosition.i][newPosition.j].positionX = (newXperc === Infinity) ? 0 : newXperc;
        newCellArray[newPosition.i][newPosition.j].positionY = (newYperc === Infinity) ? 0 : newYperc;
        return {...state,cellArray: newCellArray}; 
    }
    if (action.type === 'UPDATE_COIN_POSITION') {
        const nextPosition = action.payload;
        let newCoinsData = { ...state.coinsData };
        newCoinsData[nextPosition.color][nextPosition.index].i = nextPosition.i;
        newCoinsData[nextPosition.color][nextPosition.index].j = nextPosition.j;
        return { ...state, coinsData: newCoinsData };
    }
    if (action.type === 'SEND_COIN_TO_HOME') {
        const coin = action.payload;
        let newCoinsData = { ...state.coinsData };
        newCoinsData[coin.color][coin.index].i = -2;
        newCoinsData[coin.color][coin.index].j = -2;
        return { ...state, coinsData: newCoinsData };
    }
    if (action.type === 'NEXT_COIN_POSITION') {
        const coin = action.payload;
        let newCoinsData = { ...state.coinsData };
        const i = newCoinsData[coin.color][coin.index].i;
        const j = newCoinsData[coin.color][coin.index].j;
        const nextPosition = getNextCellIndex(i,j,coin.color);
        newCoinsData[coin.color][coin.index].i = nextPosition.i;
        newCoinsData[coin.color][coin.index].j = nextPosition.j;
        return { ...state, coinsData: newCoinsData };
    }
    if (action.type === 'CHECK_FOR_STRIKE') {
        let newCoinsData = { ...state.coinsData };
        let { color } = action.payload;
        let { index } = action.payload;
        let i = newCoinsData[color][index].i;
        let j = newCoinsData[color][index].j;
        const positionToCheck = { i, j };
        let colorToStrike = false, strikeColorCount = 0,strikeColorIndex=-1;
        ['red','green','yellow','blue'].map((COLOR)=>{
            if(COLOR === color)return true;
            newCoinsData[COLOR].map((position,index)=>{
                if(position.i===positionToCheck.i && position.j === positionToCheck.j)
                {
                    colorToStrike = COLOR;
                    strikeColorCount++;
                    strikeColorIndex = index;
                }
                return true;
            });
            return true;
        });
        if (colorToStrike && strikeColorCount%2!==0)
        {
            const cellArray = { ...state.cellArray};
            const status = cellArray[positionToCheck.i][positionToCheck.j];
            if (!status.star && !status.startCell)
            {
                newCoinsData[colorToStrike][strikeColorIndex].i = -2;
                newCoinsData[colorToStrike][strikeColorIndex].j = -2;
            }
            console.log('STRIKE');
        }
        return { ...state, coinsData: newCoinsData };
    }
    if (action.type === 'UPDATE_ALL_COINS') {
        return { ...state, coinsData: action.payload };
    }
    if (action.type === 'SAVE_ROOM_ID') {
        return { ...state, roomId: action.payload };
    }
    if (action.type === 'CHANGE_PROPERTY') {
        let newState = { ...state };
        newState[action.payload.propertyName] = action.payload.value;
        return newState;
    }
    if (action.type === 'NEXT_PLAYER_TURN') {
        let newState = { ...state };
        var COLORS = newState.usersInRoom.map((user) => { return (user.color) });
        COLORS = COLORS.filter((clr)=>{
            return (!isAllWon(newState.coinsData, clr))
        });
        let currentColor = newState.currentColor;
        let nextColorIndex = COLORS.indexOf(currentColor)+1;
        if (nextColorIndex > (COLORS.length-1)) nextColorIndex = 0;
        let nextColor = COLORS[nextColorIndex];
        newState.currentColor = nextColor;
        newState.canCoinsMove = false;
        newState.diceValue = 0;
        return newState;
    }
    if (action.type === 'SEND_MOVE_DATA_SOCKET') {
        const user = localData.getData('user');
        let diceValue = state.diceValue;
        socket.emit(state.roomId, { user: user, startMove: { ...action.payload, diceValue} });
        return state;
    }
    if (action.type === 'SEND_DATA_TO_SOCKET') {
        let coinsData = { ...state.coinsData };
        const user = localData.getData('user');
        socket.emit(state.roomId, { user: user, coinsData: coinsData });
        return state;
    }
    return state;
}

export default rootReducer;