var express = require('express');
var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var uuidv4 = require('uuid/v4');
var session = require("express-session")({
  secret: "my-secret",
  resave: true,
  saveUninitialized: true
});
var sharedsession = require("express-socket.io-session");

console.clear();

app.use(session);

app.use(express.static('../build'));

var rooms = {};

var usersOnline = [];

var COLORS = ['red', 'green', 'yellow', 'blue'];

io.use(sharedsession(session));

io.on('connection', function (socket) {
  const sessionID = socket.id;
  
  socket.on('disconnect', function () {
    var currentUser = socket.handshake.session.userdata;
    if (!currentUser)return;
    var indexToRemove = usersOnline.findIndex(function(user){
      return (user.id === currentUser.id);
    });
    if (indexToRemove>=0)
    {
      usersOnline.splice(indexToRemove,1);
    }
    userTabs = usersOnline.filter((user) => { return user.id === currentUser.id }).length;
    console.log('Disconnected');
    if (!userTabs)
    {
      delete socket.handshake.session.userdata;
      socket.handshake.session.save();
    }
    io.emit('usersList', usersOnline);
  });

  socket.on('newUser', function (userData) {
    var user = userData.user;
    var foundIndex = usersOnline.findIndex(function (USER) {
      return (USER.id === user.id);
    });
    if (foundIndex<0)
    {
      usersOnline.push(user);
    }
    var userRoomId = findUserRoom(user.id);

    var numOfRooms = Object.keys(rooms).length;

    if (!userRoomId && !numOfRooms)
    {
      userRoomId = uuidv4();
      var color = makeColor(userRoomId);
      rooms[userRoomId] = { users: [{ id: user.id,name:user.name, color }], currentColor: color, coinsData: userData.coinsData};
    }
    else if (!userRoomId)
    {
      userRoomId = Object.keys(rooms)[0];
      var color = makeColor(userRoomId);
      if (rooms[userRoomId].users.length < 3)
      {
        rooms[userRoomId].users.push({ id: user.id, name: user.name, color });
      }
      else
      {
        rooms[userRoomId].users.splice(1, 0, { id: user.id, name: user.name, color });
      } 
    }
    socket.on(userRoomId, function (data) {
      if (data.coinsData) {
        rooms[userRoomId].coinsData = data.coinsData;
        rooms[userRoomId].currentColor = data.currentColor;
      } 
      else
      {
        io.emit(userRoomId, data);
      }
    });

    io.emit(userRoomId, { user, usersInRoom: rooms[userRoomId].users});
    
    io.emit(user.id, { roomId:userRoomId, roomData: rooms[userRoomId]});

    socket.handshake.session.userdata = user;
    socket.handshake.session.save();
    io.emit('usersList', usersOnline);
  });

});

function makeColor(roomId)
{
  var room = rooms[roomId];
  if (!room)
  {
    return COLORS[Math.round(Math.random() * 3)];
  }
  var colorsInRoom = room.users.map((user) => {
    return (user.color);
  });
  var indexOfHostColor = COLORS.indexOf(colorsInRoom[0]);
  if(colorsInRoom.length === 3)
  {
    return COLORS.filter((color) => { return (!colorsInRoom.includes(color))})[0];
  }
  return COLORS[(indexOfHostColor + (colorsInRoom.length+1)) % 4];
}

function findUserRoom(userId)
{
  var userRoomId = false;
  for (const roomId in rooms) {
    if (rooms[roomId].users.findIndex((user) => { return (user.id===userId)}) >= 0) {
      userRoomId = roomId;
    }
  }
  return userRoomId;
}

http.listen(3500, function () {
  console.log('listening on *:3500');
});